# `partial-result`

The `partial-result` crate is a Rust library that provides a type representing a partial success, i.e., a result that can contain a failure. This is useful when you need to return a result and a failure, where the failure represents a non-fatal error.

## Installation

Add this to your `Cargo.toml`:

```toml
[dependencies]
partial-result = "0.1.0"
```

Then run `cargo build` to download and compile the crate.

## Usage

Here's a basic example of how to use the `PartialResult` type:

```rust
use partial_result::{
    PartialResult,
    PartialResultExt,
    PartialSuccess,
};

#[derive(Debug)]
enum CriticalError {
    WeAreDoomed(String),
    EverythingIsLost(String),
}

#[derive(Debug)]
enum NonCriticalError {
    SomethingWentWrong(String),
    SomethingElseWentWrong(String),
}

fn do_something() -> PartialResult<u32, NonCriticalError, CriticalError> {
    let value = 42;
    let failure = NonCriticalError::SomethingWentWrong("Something went wrong".to_string());

    PartialResult::partial_success(value, failure)
}

fn main() -> Result<(), CriticalError> {
    let result = do_something()?;
    println!("Result: {}", result.value);
    result.failure.map(|e| println!("WARN: there was an issue during the computation: {:?}", e));

    Ok(())
}
```

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
